<?php

class MultilangHelper
{
	public static function enabled()
	{
//		return false;
		return !Yii::app()->params['console'] && count(Yii::app()->params['translatedLanguages']) > 1;
	}

	public static function processLangInUrl($url)
	{
		if (self::enabled())
		{
			$domains = explode('/', ltrim($url, '/'));

			$isLangExists = in_array($domains[0], array_keys(Yii::app()->params['translatedLanguages']));
			$isDefaultLang = $domains[0] == Yii::app()->params['defaultLanguage'];

			if ($isLangExists && !$isDefaultLang)
			{
				$lang = array_shift($domains);
				Yii::app()->setLanguage($lang);
			}

			$url = '/' . implode('/', $domains);
		}

		return $url;
	}

	public static function addLangToUrl($url)
	{
		if (self::enabled()) {
			$domains = explode('/', ltrim($url, '/'));
				if (!in_array($domains[0], ['http:', 'https:'])) {
				$isHasLang = in_array($domains[0], array_keys(Yii::app()->params['translatedLanguages']));
				$isDefaultLang = Yii::app()->language == Yii::app()->params['defaultLanguage'];

				if ($isHasLang && $isDefaultLang)
					array_shift($domains);

				if (!$isHasLang && !$isDefaultLang)
					array_unshift($domains, Yii::app()->language);

				$url = '/' . implode('/', $domains);
			}
		}
		return $url;
	}
}