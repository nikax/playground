<?php
/**
 * класс помощник для работы с базой
 * обращение к методам MyDb::functonName();
 * в конфиге в impot необходимо подключить импорт хелперов 'application.helpers.*'
 */
class MyDb {

	/**
	 * @param $tableName string имя таблицы без приставки
	 * @param $fields array имена обновляемых полей
	 * @param $values array вставляемых значений
	 * @return mixed
	 */
	public static function multiRowsSingleQuery4PDO($tableName, $fields, $values)
	{
		$connection = Yii::app()->db;
		$insert_values = $question_marks = array();

		foreach ($values as $v)
		{
			$question_marks[] = '('.self::placeholders(sizeof($v)).')';
			foreach ($v as $a)
				$insert_values[] = $a;
		}

		$stmt = $connection->createCommand(
			"INSERT INTO {$tableName} (".implode(",", $fields).") VALUES ".implode(',', $question_marks)
		);

		return $stmt->execute($insert_values);
	}

	/**
	 * @static подготавливает строку для мультистрочной вставки с помощью PDO
	 * @param int $count int кол-во обновляемых полей
	 * @param string $text текст placeholder'a
	 * @param string $separator string разделитель между Values'ами
	 * @return string
	 */
	public static function placeholders($count = 0, $text = '?', $separator = ",")
	{
		$result = array();
		if ($count > 0)
			for ($x = 0; $x < $count; $x++)
				$result[] = $text;

		return implode($separator, $result);
	}

	/**
	 * получение уникального поля
	 * @param string $table - название таблицы
	 * @param string $field - поле таблицы
	 * @param string $word - уникальная фраза
	 * @param string $separator - разделитель, подставляемый в фразу
	 * @return bool|string
	 */
	public static function getUniqField($table, $field, $word, $separator = '-')
	{
		if ($res = Yii::app()->db->createCommand(<<<SQL
SELECT
SUBSTRING_INDEX({$table}.{$field}, '{$separator}', -1) AS right_part
FROM {$table}
WHERE
	{$table}.{$field} LIKE :word AND
	id_user = :id_user
FOR UPDATE
SQL
		)->query(array(':word' => "{$word}%", ':id_user' => Yii::app()->config->get('id_user'))))
		{
			$right = array();
			while ($row = $res->read())
				$right[] = $row['right_part'];

			if ($right)
			{
				$i = 1;
				while (in_array($i, $right))
					$i++;

				return "{$word}{$separator}{$i}";
			} else return $word;
		} else return false;
	}

	/**
	 * вставка записей из csv файла в базу
	 * @param string $file             - путь/к/файлу/файл.csv
	 * @param string $table            - имя таблицы
	 * @param bool   $isTruncateBefore - нужно ли очистить таблицу перед вставкой
	 *
	 * @return array|bool
	 */
	public static function csvFile2Table($file, $table, $isTruncateBefore = true)
	{
		# 1) файл ваще существует?
		if (!is_file($file))
			return array('error' => 'Не удалось найти файл импорта.');

		# 2) очищаем табличку, если нужно
		if ($isTruncateBefore)
		{
			if ($lines = file($file) AND count($lines) > 1)
				Yii::app()->db->createCommand("TRUNCATE TABLE `{$table}`")->execute();
			else return array('error' => 'Не прочесть файл импорта.');
		}

		# 3) вставка данных
		if (!$inserted = self::loadDataInFile($file, $table))
			return array('error' => 'Не удалось вставить строки в таблицу. '.mysql_error());

		return $inserted;
	}

	/**
	 * вставка данных в базу из файла
	 * @param        $file        string - полный путь к файлу
	 * @param        $table       string - имя таблицы
	 * @param int    $ignoreLines - сколько первых строк пропустить. зачастую 1-я строка содержит имена полей
	 * @param string $charset     string - кодировка файла
	 *
	 * @return mixed int - кол-во вставленных записей
	 */
	public static function loadDataInFile($file, $table, $ignoreLines = 1, $charset = 'utf8')
	{
		$file = str_replace('\\', '/', $file);
		$ignoreLines = (int)$ignoreLines;

		$local = (Yii::app()->params['is_prodaction']) ? 'LOCAL' : '';

		return Yii::app()->db->createCommand("
		LOAD DATA {$local} INFILE '{$file}'
		INTO TABLE {$table}
		CHARACTER SET $charset
		FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
		LINES TERMINATED BY '\r\n'
		".($ignoreLines ? "IGNORE {$ignoreLines} LINES" : ''))->execute();
	}

	/**
	 * экранирование параметра при вставке через csv
	 * @param $string
	 *
	 * @return string
	 */
	public static function clear($string)
	{
		return addslashes(str_replace(array("\r\n", "\n", "\r"), array(' ', ' ', ' '), $string));
	}

	public static function addRow($modelName, $attributes)
	{
		# validate attributes
		if (empty($attributes) || !is_array($attributes) || empty($attributes['id']))
			throw new CException("Invalid attributes: {$modelName}");

		# get the model
		if (!$model = $modelName::model()->findByPk($attributes['id']))
			$model = new $modelName();

		# update attributes
		foreach ($attributes as $key => $value) {
			if ($model->hasAttribute($key)) {
				if (is_array($value) && isset($value['expression'])) {
					$value = new CDbExpression($value['expression']);
				}
				$model->$key = $value;
			}
		}

		# save the model
		if (!$model->save(false))
			throw new CException("Cannot save [{$model->id}]");

		return $model;
	}

	public static function deleteRow($modelName, $id)
	{
		if (empty($id)) {
			throw new CException("Empty ID to delete in model: {$modelName}");
		}

		# get the model
		if ($model = $modelName::model()->findByPk($id)) {
			$model->delete();
		}
	}
}