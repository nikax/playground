<?php

class WebModule extends CWebModule
{
	public $defaultController='general';
	public $ignoreHttpsRedirect=false;

	public function init()
	{
		parent::init();
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
} 