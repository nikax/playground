<?php $this->controller->jsInit('appWidgetsMenuTop', 'init') ?>

<div class="app-widgets-menu-top js-appmain-menu-depend-marker<?= Yii::app()->user->isGuest || $this->layoutPreviewMode ? ' menu-outside' : '' ?>">
	<ul>
		<?php foreach($items as $item): ?>
			<?php if ($item->attributes['visible']): ?>
				<?= CHtml::openTag('li', ['class'=>$item->attributes['active'] ? $item->attributes['liClass'].' active-menu-item' : $item->attributes['liClass']]) ?>

					<?= CHtml::openTag('a', array_merge($item->attributes['htmlOptions'], ['href'=>$item->attributes['url']])) ?>

					<span><?= $item->attributes['label'] ?></span>

					<?php if ($item->attributes['cntPrimary']): ?>
						<span class="cnt-primary"><?= $item->attributes['cntPrimaryFormated'] ?></span>
					<?php endif ?>

					<?= CHtml::closeTag('a') ?>

					<?php if ($item->attributes['children'] && !Yii::app()->user->isGuest): ?>
						<div class="b-mt-child b-mt-child-<?= $item->attributes['htmlOptions']['data-menu-name'] ?> js-mt-child js-mt-child-<?= $item->attributes['htmlOptions']['data-menu-name'] ?><?= $item->attributes['children']['right-align'] ? ' m-right':'' ?> hidden" data-name="<?= $item->attributes['htmlOptions']['data-menu-name'] ?>">
							<?php if (!empty($item->attributes['children']['widget'])): ?>
								<?php $this->controller->widget($item->attributes['children']['widget']) ?>
							<?php endif ?>
						</div>
					<?php endif ?>

				<?= CHtml::closeTag('li') ?>
			<?php endif ?>
		<?php endforeach ?>
	</ul>
</div>