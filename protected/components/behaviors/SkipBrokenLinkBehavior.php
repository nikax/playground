<?php

class SkipBrokenLinkBehavior extends CActiveRecordBehavior
{
	private $_findedBrokenLink = [];
	public $isSkipBl = [];

	public function getFindedBrokenLink($attribute)
	{
		return empty($this->_findedBrokenLink[$attribute]) ? false : $this->_findedBrokenLink[$attribute];
	}

	public function setFindedBrokenLink($findedBrokenLink, $attribute)
	{
		$this->_findedBrokenLink[$attribute] = $findedBrokenLink;
	}

	public function exceptBrokenLink($attribute)
	{
		$class = get_class($this->getOwner());
		$this->isSkipBl = empty($_POST[$class]['isSkipBl']) || !is_array($_POST[$class]['isSkipBl']) ? [] : $_POST[$class]['isSkipBl'];
		return in_array($attribute, $this->isSkipBl) ? $this->getOwner()->scenario : '';
	}
}