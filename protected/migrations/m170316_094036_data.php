<?php
class m170316_094036_data extends CDbMigration
{
	private $sql = array();

	public function up()
	{
		$this->sql[]=<<<SQL
INSERT INTO `niches` (`id`,`name`,`default`) values

(1,'Marketing',1),

(2,'Finance',0),

(5,'Health',0),

(9,'Mobile',0),

(13,'Social',0);

INSERT INTO `email_category` (`id`,`name`,`sname`,`ord`) values

(1,'Buyers','buyer',1),

(2,'Sellers','seller',2),

(4,'Miscellaneous','miscellaneous',4),

(5,'System','system',5),

(7,'Affiliates','affiliates',3);

INSERT INTO `email_type` (`id`,`name`,`id_category`,`active`,`editable`,`ord`) values

(4,'Order accepted',1,1,1,10),

(5,'Order rejected',1,1,1,20),

(6,'Solo has been canceled',1,1,1,30),

(7,'Solo was refunded',1,1,1,40),

(9,'New order',2,1,1,10),

(10,'Order was retracted',2,1,1,20),

(11,'Solo has been canceled',2,1,1,30),

(12,'Solo has been refunded',2,1,1,40),

(13,'You missed new order',2,1,1,25),

(15,'Your solo deal dropped off the first page',2,1,1,90),

(18,'Private message',4,1,1,NULL),

(19,'Cron Error',5,1,0,NULL),

(20,'Restore password',5,1,0,NULL),

(21,'Registration',5,1,0,NULL),

(23,'Support answer',5,1,0,NULL),

(24,'Reply in forum thread',5,1,0,NULL),

(26,'Udimi news and weekly digest',4,1,1,NULL),

(27,'User after registration',5,1,0,NULL),

(28,'User idle',5,1,0,NULL),

(29,'Buyer first bought solo',5,1,0,NULL),

(30,'Solo completed notice',1,1,1,NULL),

(31,'Solo completed',2,1,1,70),

(32,'Support escalate',5,1,0,NULL),

(33,'Support question',5,1,0,NULL),

(34,'Click delivery started',1,1,1,32),

(39,'Create new forum thread',5,1,0,NULL),

(40,'Special offer',5,1,0,NULL),

(41,'Reply by email on PM',5,1,0,NULL),

(42,'Swipe or link change request',2,1,1,65),

(43,'Seller rejected new swipe or link',1,1,1,34),

(44,'Seller has accepted swipe or link change request',1,1,1,NULL),

(45,'Swipe or link change request expired',1,1,1,38),

(46,'Discount expiry notices',1,1,1,100),

(47,'New referral registered',7,1,1,NULL),

(48,'New affiliate commission',7,1,1,NULL),

(49,'Recurring payments',5,1,0,NULL),

(50,'Custom affiliate agreements',7,1,1,NULL),

(51,'Refund to credit card',5,1,0,NULL);

INSERT INTO `email_template` (`id`,`id_type`,`name`,`view`,`subject`,`is_phpSubject`,`notes`) values

(4,21,'register','system.register','Confirm your email',0,''),

(5,20,'forgot','system.forgot','Password reset',0,''),

(8,18,'pm','miscellaneous.pm','\"{\$model->idPartner->fullname} sent you a message\"',1,''),

(10,26,'support_answer','system.support_answer','\"{\$subject}\"',1,''),

(11,24,'forum_new_comment','system.forum_new_comment','\'Re: \'.CHtml::encode(\$modelComment->idPost->nameDisplay)',1,''),

(12,4,'on_accept','buyers.on_accept','\$model->idSeller->fullname.\' accepted your order on \'.date(\"d M Y\", strtotime(\$model->solo_dta))',1,''),

(13,5,'on_reject','buyers.on_reject','Seller has rejected your order',0,''),

(14,11,'on_cancelbybuyer','sellers.on_cancelbybuyer','\'Buyer has cancelled your solo on \'.date(\"d M Y\", strtotime(\$model->solo_dta))',1,''),

(15,6,'on_cancelbyseller','buyers.on_cancelbyseller','\'Seller has cancelled your solo on \'.date(\"d M Y\", strtotime(\$model->solo_dta))',1,''),

(16,7,'on_refund_buyer','buyers.on_refund_buyer','Solo order has been refunded',0,''),

(17,7,'on_fail_buyer','buyers.on_fail_buyer','Solo order has been refunded',0,''),

(18,12,'on_refund_seller','sellers.on_refund_seller','Solo order has been refunded',0,''),

(19,12,'on_fail_seller','sellers.on_fail_seller','Solo order has been refunded',0,''),

(21,9,'on_create','sellers.on_create','\'Good News! New solo order for  \'.\$model->order_clicks.\' clicks\'',1,''),

(22,10,'on_retracted','sellers.on_retracted','Buyer has retracted the solo order',0,''),

(23,13,'on_expired','sellers.on_expired','Solo order has expired',0,''),

(25,15,'bump_recomend','sellers.bump_recomend','\'Your \'.\$name.\' thread has been dropped off the first page\'',1,''),

(28,26,'digest','miscellaneous.digest','\"{\$subject}\"',1,''),

(29,26,'welcome','system.welcome','Welcome to Udimi!',0,''),

(30,26,'user-idle','system.user_idle','No activity after your registration',0,''),

(31,26,'buyer-first-order','system.buyer_first_order','Congratulations on your first order!',0,''),

(32,30,'clicks_delivered_buyer','buyers.clicks_delivered','Good news! Your solo has been delivered!',0,''),

(33,30,'clicks_not_delivered_buyer','buyers.clicks_not_delivered','You got a refund for undelivered solo',0,''),

(34,31,'clicks_delivered_seller','sellers.clicks_delivered','Good news! You delivered solo on time!',0,''),

(35,31,'clicks_not_delivered_seller','sellers.clicks_not_delivered','You failed to deliver your solo',0,''),

(36,32,'support_escalate','system.support_escalate','\"{\$subject}\"',1,''),

(37,26,'support_question','system.support_question','\"{\$subject}\"',1,''),

(38,34,'clicks_delivery_started_buyer','buyers.clicks_delivery_started','\"Delivery started by {\$soloModel->idSeller->fullname}\"',1,''),

(43,26,'forum_new_post','system.forum_new_post','\'Forum: \'.CHtml::encode(\$modelComment->idPost->nameDisplay)',1,''),

(44,26,'special_offer','system.special_offer','\"{\$model->idUser->fullname} sent you a Special Offer\"',1,''),

(45,41,'reply_pm','system.reply_pm','Problem with your message',0,''),

(46,42,'swch_request_new','sellers.swch_request_new','\"{\$adType} change request\"',1,''),

(47,43,'swch_request_rejected','buyers.swch_request_rejected','\"{\$adType} change request rejected\"',1,''),

(48,44,'swch_request_accepted','buyers.swch_request_accepted','\"{\$adType} change request accepted\"',1,''),

(49,45,'swch_request_expired','buyers.swch_request_expired','\"{\$adType} change request expired\"',1,''),

(50,46,'discount_expire','buyers.discount_expire','\"Your \\\${\$discountAmount} discount expires tomorrow\"',1,''),

(51,47,'referral_registered','affiliates.referral_registered','\"New referral registered: {\$referral->fullname}\"',1,''),

(52,48,'commission_received','affiliates.commission_received','\"You received affiliate commission of $\".number_format(\$commission, 2)',1,''),

(53,49,'creditcard_recurring_fail','system.creditcard_recurring_fail','Receipt for Invoice: Recurring payment failed.',0,''),

(54,49,'creditcard_success','system.creditcard_success','\"Receipt for Invoice. Login: {\$recipient->email}\"',1,''),

(55,50,'aff_agreements_created','affiliates.aff_agreements_created','\"Special agreement created by {\$idSeller->fullname}\"',1,''),

(56,50,'aff_agreements_cancelled','affiliates.aff_agreements_cancelled','\"Special agreement cancelled by {\$idSeller->fullname}\"',1,''),

(57,51,'refund_to_creditcard','system.refund_to_creditcard','Your purchase has been refunded by Udimi',0,''),

(59,26,'helper_msg_to_user','system.helper_msg_to_user','\"{\$subject}\"',1,''),

(60,26,'helper_msg_to_admin','system.helper_msg_to_admin','\"{\$subject}\"',1,'');
SQL;
		$this->executeSql();
	}

	public function down()
	{
		echo "m170316_094036_data does not support migration down.\\n";
		return false;
	}

	private function executeSql()
	{
		if (!empty($this->sql)) {
			foreach ($this->sql as $sql) {
				$this->execute($sql);
			}
		}
	}
}