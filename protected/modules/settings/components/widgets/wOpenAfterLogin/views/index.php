<?php $form = $this->beginWidget(
	'CActiveForm', array(
		'htmlOptions' => array(
			'class' => 'form-horizontal',
		))) ?>

	<?= CHtml::dropDownList(
		'after_login_redirect',
		Yii::app()->userConfig->get('after_login_redirect', ''),
		Yii::app()->user->getAfterLoginRedirectOptions(),
		[
			'class'=>"form-control js-oal-autosubmit",
			'data-init'=>"chosen-list",
			'data-width'=>'100px',
			'id'=>false,
		]
	) ?>

	<button type="submit" class="hidden js-oal-submit auto-enable ajax-post" data-href="<?= $this->controller->createUrl('wopenafterlogin.save')?>" data-widget="wopenafterlogin"></button>

<?php $this->endWidget() ?>