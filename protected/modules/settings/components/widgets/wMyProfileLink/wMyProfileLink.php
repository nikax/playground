<?php
class wMyProfileLink extends Widget
{
	public function init()
	{
		// Обязательный вызов.
		parent::init(__CLASS__);

		$this->controller->jsInit('settingsGeneral', 'initMyProfileLink');
	}

	/**
	 * @return string|void Классический метод виджета
	 */
	public function run()
	{
		// Обязательно указываем return, чтобы виджет работал как при обычной загрузке страницы, так и через аякс
		return $this->render('index');
	}

	/**
	 * @return array Регистрируем actions
	 *
	 * Пример регистрации базового класса WidgetBaseAction в качестве экшна перерисовки виджета:
	 * 'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
	 * в яваскрипте доступна функция appMain.ajaxLoadWidgetUrl('wmenuleft', 'reload')
	 * где wmenuleft - имя класса виджета, а reload - имя экшна (ключ массива actions)
	 *
	 * Пример регистрации произвольного класса widget action:
	 * 'example'=>array('class'=>__CLASS__.'.actions.Example', 'widget_alias'=>__CLASS__),
	 * Необходимо создать класс в директории actions в папке виджета: class Example extends WidgetBaseAction
	 * в классе доступен метод получения экземпляра текущего виджета с его параметрами: $this->getWidgetInstance()
	 * в шаблоне обращение к такому action выглядит следующим образом:
	 * <a href="<?php echo $this->createUrl('wmenuleft.example', array('x'=>5)) ?>" data-widget="wmenuleft" class="ajax-get">Load</a>
	 */
	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
			'save'=>array('class'=>__CLASS__.'.actions.Save', 'widget_alias'=>__CLASS__),
		);
	}
}