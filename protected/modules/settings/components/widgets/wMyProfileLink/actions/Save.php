<?php
class Save extends WidgetBaseAction
{
	public function run()
	{
		$model = clone Yii::app()->user->model;
		$model->setScenario('settingsProfileLink');

		if (isset($_POST[get_class($model)])) {
			$oldValue = $model->uid_profile;
			$model->attributes = $_POST[get_class($model)];
			if ($model->validate()) {
				if ($oldValue != $model->uid_profile) {
					$model->save(false);
				}
			} else {
				$this->controller->jsonResponse(array('error'=>MyUtils::getFirstError($model)));
			}

			$widget = $this->getWidgetInstance();
			// Response
			$this->controller->jsonResponse(
				array(
					'container' => '#'.$widget->containerId,
					'content' => $widget->run(),
					'callback' => array(
						$oldValue != $model->uid_profile ? 'appMain.showToast("Settings saved", "success")':'',
						"appMain.ajaxLoadWidgetUrl('wmyprofilelink', 'reload')"
					),
				)
			);
		}
	}
}