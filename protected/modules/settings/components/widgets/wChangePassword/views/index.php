<?php $form = $this->beginWidget(
	'CActiveForm', array(
		'id' => 'changepassword-form',
		'htmlOptions' => array(
			'data-init' => 'dirty-check',
			'class' => 'form-horizontal',
		),
	)) ?>

	<div class="form-group">
		<?= $form->labelEx($model, 'passwordOld', array('class'=>'control-label col-sm-4 required-hide')) ?>
		<div class="col-sm-5">
			<?= $form->passwordField($model, 'passwordOld', array('class'=>'form-control')) ?>
		</div>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'passwordNew', array('class'=>'control-label col-sm-4 required-hide')) ?>
		<div class="col-sm-5">
			<?= $form->passwordField($model, 'passwordNew', array('class'=>'form-control')) ?>
		</div>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'passwordNewConfirm', array('class'=>'control-label col-sm-4 required-hide')) ?>
		<div class="col-sm-5">
			<?= $form->passwordField($model, 'passwordNewConfirm', array('class'=>'form-control')) ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4">
			<button type="submit" class="btn btn-primary ajax-post" data-href="<?= $this->controller->createUrl('wchangepassword.save')?>" data-widget="wchangepassword">Change password</button>
		</div>
	</div>

	<?php $this->endWidget() ?>