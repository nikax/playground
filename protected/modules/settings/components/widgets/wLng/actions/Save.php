<?php
class Save extends WidgetBaseAction
{
	public function run()
	{
		if (isset($_POST['Users'])) {

			$model = Users::model()->findByPk(Yii::app()->user->id);
			$model->setScenario('settingsLng');
			$model->attributes = $_POST['Users'];
			if ($model->validate()) {
				$model->save(false);
			} else {
				$this->controller->jsonResponse(array('error'=>MyUtils::getFirstError($model)));
			}

			$widget = $this->getWidgetInstance();
			// Response
			$this->controller->jsonResponse(
				array(
					'soft_redirect' => '',
					'callback' => array(
						'appMain.showToast("Settings saved", "success")',
					),
				)
			);
		}
	}
}