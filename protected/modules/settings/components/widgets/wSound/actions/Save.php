<?php
class Save extends WidgetBaseAction
{
	public function run()
	{
		if (isset($_POST['Users'])) {
			$widget = $this->getWidgetInstance();
			$model = Yii::app()->user->model;

			$model->setScenario('settingsSound');
			$model->attributes = $_POST['Users'];
			if ($model->validate()) {
				$model->save();
			}  else {
				$this->controller->jsonResponse(array('error'=>MyUtils::getFirstError($model)));
			}
			// Response
			$this->controller->jsonResponse(
				array(
					'callback' => array(
						'appMain.showToast("Sound settings updated", "success")',
						'settingsGeneral.initSound()',
					),
					'container' => '#'.$widget->containerId,
					'content' => $widget->run(),
				)
			);
		}
	}
}