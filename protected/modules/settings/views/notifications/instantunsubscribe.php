<?php $this->beginContent('/layouts/main'); ?>
<div class="settings-notifications">
	<p class="b-header">Successfully Unsubscribed</p>

	<p>Succesfully unsubscribed <?=$user->email;?> from <?php echo count($models)==1 ? '"'.$models[0]->name.'"' : 'all'; ?> notifications</p>

	<?php if (count($models)==1):?>
		<p><a href="<?=$this->createUrl('instantunsubscribe', ['hash'=>$user->hash, 'it'=>'all']);?>" class="ajax-get">Click here to unsubscribe from all notifications</a></p>
	<?php endif;?>

	<p><a href="<?php echo $this->guestUserModel ? $this->createUrl('index', array('hash'=>$user->hash)) : $this->createUrl('index') ?>" class="ajax-get">View active subscriptions</a></p>

</div>
<?php $this->endContent(); ?>
