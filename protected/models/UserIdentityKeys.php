<?php
class UserIdentityKeys extends ActiveRecord
{
	/**
	 * Время жизни ссылки восстановления пароля (в днях)
	 */
	const FORGONT_PASSWORD_TTL = 1;

	/**
	 * Время жизни ссылки подтверждения регистрации (в часах)
	 */
	const CONFIRM_REGISTER_TTL = 24;

	/**
	 * Время жизни ссылки логина админа под аккаунтом юзера (в секундах)
	 */
	const CONTROL_LOGIN_TTL = 60;

	/**
	 * Время запоминания юзера в куках при логине в систему (в днях)
	 */
	const COOKIE_LOGIN_TTL = 30;

	/**
	 * Время жизни токена логина для мобильных приложений (в днях)
	 */
	const MOBILE_LOGIN_TTL = 30;

	public function tableName()
	{
		return 'user_identity_keys';
	}

	public function relations()
	{
		return [
			'idUser' => [self::BELONGS_TO, 'Users', 'id_user'],
		];
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Строим аргумент для метода strtotime
	 * @param $type
	 *
	 * @throws CException
	 */
	public static function getTTL($type)
	{
		switch ($type) {
		case 'forgot_password':
			return '+' . self::FORGONT_PASSWORD_TTL . ' day';
			break;
		case 'confirm_email':
			return '+' . self::CONFIRM_REGISTER_TTL . ' hour';
			break;
		case 'control_login':
			return '+' . self::CONTROL_LOGIN_TTL . ' second';
			break;
		case 'cookie_login':
			return '+' . self::COOKIE_LOGIN_TTL . ' day';
			break;
		case 'mobile_login':
			return '+' . self::MOBILE_LOGIN_TTL . ' day';
			break;
		default:
			throw new CException('param "type" contain unsupported value');
		}
	}

	/**
	 * Создать ключ авторизации
	 * @param $userModel
	 * @param $type
	 *
	 * @throws CException
	 */
	public static function create($userModel, $type, $refresh = false, $duplicate = false, $strong = false)
	{
		$model = self::model()->findByAttributes([
			'id_user' => $userModel->id,
			'tp' => $type,
		]);

		if (!empty($model) && $refresh && !$duplicate) {
			// перегенерировать ключ
			$model->key = self::getUniqKey($strong);

		} elseif (empty($model) || strtotime($model->dta) < time() || $duplicate) {
			// создать новый ключ
			$class = ucfirst(get_class());
			$model = new $class;
			$model->id_user = $userModel->id;
			$model->tp = $type;
			$model->key = self::getUniqKey($strong);
		}

		// обновить время жизни ключа
		$model->dta = date("Y-m-d H:i:s", strtotime(self::getTTL($type)));

		$model->save();
		return $model->key;
	}

	/**
	 * Удалить ключ авторизации
	 * @param $userModel
	 * @param $type
	 *
	 * @throws CException
	 */
	public static function remove($userModel, $type)
	{
		$model = self::model()->findByAttributes(
			[
				'id_user' => $userModel->id,
				'tp' => $type,
			]
		);
		if (!empty($model)) {
			$model->delete();
		}
	}

	private static function getUniqKey($strong = false)
	{
		return MyUtils::generateUniqueStringUid(
			[
				'length' => $strong ? 32 : 8,
				'allowed_chars' => $strong
					? 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
					: '123456789',
			],
			'user_identity_keys',
			'key'
		);
	}

	public static function deleteExpired($id_user)
	{
		$expired = UserIdentityKeys::model()->findAll(
			'id_user = :user AND tp = :tp AND NOW() > DATE_ADD(dta, INTERVAL :ttl DAY)', [
				':user' => $id_user,
				':tp' => 'cookie_login',
				':ttl' => self::COOKIE_LOGIN_TTL,
			]
		);
		foreach ($expired as $item) {
			$item->delete();
		}
	}
}