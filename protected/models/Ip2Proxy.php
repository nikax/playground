<?php
class Ip2Proxy extends ActiveRecord
{

	public function tableName()
	{
		return 'ip2_proxy';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getRow($ip=false)
	{
		$ip = $ip ? $ip : (empty($_SERVER['REMOTE_ADDR']) ? '' : $_SERVER['REMOTE_ADDR']);
		$sql = "SELECT * FROM
				(SELECT * FROM ip2_proxy WHERE `ip_from` <=INET_ATON(:ip) ORDER BY `ip_from` DESC LIMIT 1) AS t
				WHERE `ip_to` >=INET_ATON(:ip)
			";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(':ip', $ip);
		return $command->queryRow();
	}

}