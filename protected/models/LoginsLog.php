<?php

class LoginsLog extends ActiveRecord
{
	/**
	 * Кол-во неудачных попыток логина, после которых выводится капча
	 */
	const ATTEMPTS = 3;

	/**
	 * Неудачные попытки логина считаются в промежутке времени "за последние LAST_TIME минут"
	 */
	const LAST_TIME = 10;

	/*
	 * количество стран из которых логинились для мониторинга
	 */
	const MONITORING_MIN_COUNTRIES = 2;

	/*
	 * количество УЧЕТОК  из которых логинились на одном и том же IP
	 */
	const MONITORING_MIN_ACCOUNTS = 2;

	public $cnt;
	public $last_dta;
	public $concat_countries;
	public $concat_accounts;

	public $ip2_data;
	public $search_location = true;

	public $used_by;

	public function tableName()
	{
		return 'logins_log';
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'idCountry' => array(self::BELONGS_TO, 'Country', 'id_country'),
			'prevIsCountry' => array(self::BELONGS_TO, 'Country', 'prev_id_country'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDataProvider(){

		$criteria = new CDbCriteria(array(
			'with' => array('idUser', 'idCountry'),
			'condition' => 'id_user=:id_user',
			'params' => array(':id_user'=> $this->id_user),
			'order' => 'dta DESC',
			'together' => true
		));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'class' => 'LoadMorePagination',
				'reverse' => false,
				'pageSize' => 25,
			),
		));
	}

	public static function isCaptchaShown()
	{
		$model = self::model()->findAll(array(
				'condition'=>'(ip=:ip) AND (dta>=:dta)',
				'params'=>array(
					':ip'=>$_SERVER['REMOTE_ADDR'],
					':dta'=>date('Y-m-d H:i:S', strtotime('-'.self::LAST_TIME.' minutes')),
				),
				'order'=>'dta DESC',
				'limit'=>self::ATTEMPTS,
			));

		// если попыток меньше положенного, то капчу не выводим апприори.
		if (sizeof($model) < self::ATTEMPTS) {
			return false;
		}

		foreach($model as $item) {
			if ($item->is_success) {
				return false;
			}
		}

		return true;
	}

	protected function afterFind()
	{
		parent::afterFind();
		if ($this->search_location)
			$this->ip2_data = Ip2Location::selectInfoLocation($this->ip);
	}

	protected function beforeSave()
	{
		if (parent::beforeSave())
		{
			if ($this->isNewRecord)
			{
				$now = date('Y-m-d H:i:s');
				$this->dta = $now;

				$browser = new Browser();
				$this->platform = $browser->getPlatform();
				$this->browser = $browser->getBrowser();
				$this->version = $browser->getVersion();

				$this->lng = MyUtils::getUserLng();
			}

			return true;
		}
		else
			return false;
	}

	public function loadPrevSuccess()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('id_user = :user AND is_success = 1');
		$criteria->params = [':user' => $this->id_user];
		$criteria->order = 'dta DESC';
		return self::model()->find($criteria);
	}

	public static function getUniqueFingerprints($id_user, $field = 'fingerprint')
	{
		if (!in_array($field, ['fingerprint', 'fingerprint2']))
			throw new CException('Invalid params');

		return Yii::app()->db->createCommand()
			->selectDistinct($field)
			->from('logins_log')
			->where('id_user=:id_user AND '.$field.' IS NOT NULL AND is_success=1', array(':id_user'=>$id_user))
			->queryColumn();
	}

	public static function getUniqueIps($id_user)
	{
		return Yii::app()->db->createCommand()
			->selectDistinct('ip')
			->from('logins_log')
			->where('id_user=:id_user AND ip IS NOT NULL AND is_success=1', array(':id_user'=>$id_user))
			->queryColumn();
	}

	public static function getLastFp($id_user)
	{
		$model = self::model()->find(array(
			'select'=>'fingerprint',
			'condition'=>'id_user=:id_user',
			'params'=>array(
				':id_user'=>$id_user,
			),
			'order'=>'dta DESC',
			'limit'=>1,
		));
		if (empty($model)) {
			return '';
		}
		return empty($model->fingerprint) ? '' : $model->fingerprint;
	}
}